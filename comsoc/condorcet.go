package comsoc

func CondorcetWinner(p Profile) (bestAlts []Alternative, err error) {
	alts := GetAlts(p)
	err = CheckProfileAlternative(p, alts)
	if err != nil {
		return nil, err
	}

	scores := make([]int, len(alts)) //nombre de duels gagnés par candidats
	for i, val := range alts {
		for j, val2 := range alts[i+1:] {
			score_i_j := 0 //nombre de personnes préférant i a j
			for _, pref := range p {
				if IsPref(val, val2, pref) {
					score_i_j += 1
				}
			}
			if float64(score_i_j) > (float64(len(p)) / 2.0) {
				scores[i]++ //si strictement plus de personnes préfèrent i a j, i gagne le duel
			}
			if float64(score_i_j) < (float64(len(p)) / 2.0) {
				scores[j+i+1]++ //inverse
			}
		}
	}

	//trouver si gagnant de Condorcet => score de len(alts)-1
	for i, val := range scores {
		if val == len(alts)-1 {
			bestAlts = append(bestAlts, alts[i])
			return bestAlts, err
		}
	}
	return bestAlts, err
}
