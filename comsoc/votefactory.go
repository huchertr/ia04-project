package comsoc

import (
	"errors"
	"fmt"
)

func SWFFactory(swf func(p Profile) (Count, error), tBreak func([]Alternative) (Alternative, error)) func(Profile) ([]Alternative, error) {

	if swf == nil || tBreak == nil {
		myError := errors.New("fonction swf ou tBreak manquante")
		fmt.Println(myError)
		return nil
	}
	swfNoExaequo := func(pp Profile) ([]Alternative, error) {

		var ordre []Alternative

		decompte, err := swf(pp)
		if err != nil {
			return nil, err
		}

		tailleDecompte := len(decompte)

		for len(ordre) != tailleDecompte {
			bestAlts := MaxCount(decompte)

			if len(bestAlts) > 1 {
				alt, err2 := tBreak(bestAlts)
				if err2 != nil {
					return nil, err
				}
				ordre = append(ordre, alt)
				delete(decompte, alt)
			} else {
				ordre = append(ordre, bestAlts[0])
				delete(decompte, bestAlts[0])
			}

		}
		return ordre, nil
	}
	return swfNoExaequo

}

func SCFFactory(scf func(p Profile) ([]Alternative, error), tBreak func([]Alternative) (Alternative, error)) func(Profile) (Alternative, error) {

	if scf == nil || tBreak == nil {
		myError := errors.New("fonction scf ou tBreak manquante")
		fmt.Println(myError)
		return nil
	}
	scfNoExaequo := func(pp Profile) (Alternative, error) {

		alts, err := scf(pp)
		if err != nil {
			return 0, err
		}
		alt, err2 := tBreak(alts)
		if err2 != nil {
			return 0, err
		}
		return alt, nil

	}
	return scfNoExaequo
}
