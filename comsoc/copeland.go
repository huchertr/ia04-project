package comsoc

func CopelandSWF(p Profile) (Count, error) {
	c := make(Count)
	alts := GetAlts(p)
	err := CheckProfileAlternative(p, alts)
	if err != nil {
		return nil, err
	}
	for i := range alts {
		c[alts[i]] = 0
	}

	for i, val := range alts {
		for _, val2 := range alts[i+1:] {
			score_i_j := 0 //nombre de votants préférant i a j
			for _, pref := range p {
				if IsPref(val, val2, pref) {
					score_i_j += 1
				}
			}

			if float64(score_i_j) > (float64(len(p)) / 2.0) {
				c[val] = c[val] + 1 //si i gagne le duel, il gagne un point, s'il perd, il perd un point
				c[val2] = c[val2] - 1
			}
			if float64(score_i_j) < (float64(len(p)) / 2.0) {
				c[val] = c[val] - 1
				c[val2] = c[val2] + 1
			}
		}
	}
	return c, err
}

func CopelandSCF(p Profile) (bestAlts []Alternative, err error) {
	count, err := CopelandSWF(p)
	if err != nil {
		return nil, err
	}
	return MaxCount(count), err
}
