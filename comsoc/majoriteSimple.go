// package typeVote
package comsoc

func MajoritySWF(p Profile) (count Count, err error) {
	alts := GetAlts(p)
	err = CheckProfileAlternative(p, alts)
	if err != nil {
		return nil, err
	}
	count = make(Count)
	//initialisation de count avec tous les candidats avec 0 points
	for i := range alts {
		count[alts[i]] = 0
	}
	//ajout d'un point si candidat est le préféré d'un votant
	for _, pref := range p {
		count[pref[0]]++
	}
	return count, nil

}

func MajoritySCF(p Profile) (bestAlts []Alternative, err error) {
	count, err := MajoritySWF(p)
	if err != nil {
		return nil, err
	}
	return MaxCount(count), err
}
