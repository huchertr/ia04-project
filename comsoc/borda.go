package comsoc

func BordaSWF(p Profile) (count Count, err error) {
	alts := GetAlts(p)
	err = CheckProfileAlternative(p, alts)
	if err != nil {
		return nil, err
	}
	count = make(Count)
	tailleMoins1 := len(alts) - 1

	for _, val := range p {
		for i, val2 := range val {
			count[val2] += tailleMoins1 - i
		}
	}
	return count, nil

}

func BordaSCF(p Profile) (bestAlts []Alternative, err error) {
	count, err := BordaSWF(p)
	if err != nil {
		return nil, err
	}
	return MaxCount(count), err

}
