package comsoc

func ApprovalSWF(p Profile, thresholds []int) (count Count, err error) {
	alts := GetAlts(p)
	err = CheckProfileAlternative(p, alts)
	if err != nil {
		return nil, err
	}
	count = make(Count)
	for i, val := range p {
		sl := val[0:thresholds[i]]
		for _, val2 := range sl {
			count[val2]++
		}

	}
	return count, nil

}

func ApprovalSCF(p Profile, thresholds []int) (bestAlts []Alternative, err error) {
	count, err := ApprovalSWF(p, thresholds)
	if err != nil {
		return nil, err
	}
	return MaxCount(count), err
}
