package comsoc

import (
	"errors"
)

func TieBreak(alt []Alternative) (Alternative, error) {

	if len(alt) == 0 {
		return 0, errors.New("slice d'alternatives vide")
	}
	return alt[0], nil

}

func TieBreakFactory(orderedAlts []Alternative) func([]Alternative) (Alternative, error) {

	if len(orderedAlts) == 0 {
		return nil
	}

	TieBreak := func(alt []Alternative) (Alternative, error) {

		if len(alt) == 0 {
			return 0, errors.New("slice d'alternatives vide")
		}

		for _, val := range orderedAlts {
			for _, val2 := range alt {
				if val2 == val {
					return val2, nil
				}
			}
		}
		return alt[0], nil
	}
	return TieBreak

}
