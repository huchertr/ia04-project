package comsoc

import (
	"errors"
	"fmt"
	"slices"
)

type Alternative int

type Profile [][]Alternative

type Count map[Alternative]int

type RequestVote struct {
	IdAgent  string `json:"agent-id"`
	IdBallot string `json:"ballot-id"`
	Prefs    []int  `json:"prefs"`
	Options  []int  `json:"options"`
}

type RequestBallot struct {
	Rule     string   `json:"rule"`
	Deadline string   `json:"deadline"`
	VoterIds []string `json:"voter-ids"`
	Alts     int      `json:"#alts"`
	TieBreak []int    `json:"tie-break"`
}

type ResponseBallot struct {
	IdBallot string `json:"ballot-id"`
}

type RequestDecompte struct {
	IdBallot string `json:"ballot-id"`
}

type ResponseDecompte struct {
	Winner int `json:"winner"`
}

// renvoie l'indice ou se trouve alt dans prefs
func Rank(alt Alternative, prefs []Alternative) int {
	for i, val := range prefs {
		if val == alt {
			return i
		}
	}
	return 0 //si alt n'appartient pas a prefs
}

// renvoie vrai ssi alt1 est préférée à alt2
func IsPref(alt1, alt2 Alternative, prefs []Alternative) bool {
	for i := range prefs {
		if prefs[i] == alt1 {
			return true
		}
		if prefs[i] == alt2 {
			return false
		}
	}
	return false
}

// renvoie les meilleures alternatives pour un décompte donné
func MaxCount(count Count) (bestAlts []Alternative) {
	max_value := 0
	for key, value := range count {
		if value > max_value {
			bestAlts = bestAlts[:0]
			bestAlts = append(bestAlts, key)
			max_value = value
		}
		if value == max_value && !slices.Contains(bestAlts, key) {
			bestAlts = append(bestAlts, key)
		}
	}
	return bestAlts
}

// vérifie les préférences d'un agent, par ex. qu'ils sont tous complets et que chaque alternative n'apparaît qu'une seule fois
func CheckProfile(prefs []Alternative, alts []Alternative) error {

	contenueBool := false

	if len(prefs) != len(alts) {
		return errors.New("pas taille identique")
	}
	for i, valP := range prefs {

		sl := prefs[i+1:]
		for _, valA := range alts {
			if valA == valP {
				contenueBool = true
			}

		}
		if !contenueBool {
			fmt.Println(valP)
			return errors.New("n'est pas contenu")
		}
		for _, valC := range sl {
			if valC == valP {
				return errors.New("doublons")
			}
		}
		contenueBool = false
	}
	return nil
}

// vérifie le profil donné, par ex. qu'ils sont tous complets et que chaque alternative de alts apparaît exactement une fois par préférences
func CheckProfileAlternative(prefs Profile, alts []Alternative) error {

	if alts == nil || prefs == nil {
		return errors.New("profile vide")
	}

	for _, val := range prefs {
		error := CheckProfile(val, alts)
		if error != nil {

			return error

		}
	}
	return nil
}

func GetAlts(prefs Profile) []Alternative {
	if prefs == nil || len(prefs) == 0 || prefs[0] == nil {
		return nil
	} else {
		return prefs[0]
	}
}
