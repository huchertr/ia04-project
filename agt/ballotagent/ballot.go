package ballot

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"sync"
	"time"

	comsoc "gitlab.utc.fr/huchertr/ia04-project/comsoc"
)

type Ballot struct {
	id           string //ballot-id
	Rule         string // 'json:"rule"'
	Deadline     string
	voterIds     []string
	VoterActual  []string // ?
	alts         []comsoc.Alternative
	TieBreak     []int
	Options      []int          //?
	Profil       comsoc.Profile //?
	sync.RWMutex                //?
}

// tester que tous les arguments soient bien renseigné ?
func NewBallot(id string, rule string, deadline string, voterIds []string, alts int, tieBreak []int) *Ballot {

	sl := make([]comsoc.Alternative, alts)
	for i := 0; i < alts; i++ {
		sl[i] = comsoc.Alternative(i + 1)
	}
	return &Ballot{id: id, Rule: rule, Deadline: deadline, voterIds: voterIds, alts: sl, TieBreak: tieBreak}
}

func (rsa *Ballot) voted(idAgent string) bool {

	for _, val := range rsa.VoterActual {

		if idAgent == val {
			return true
		}
	}
	return false
}

// to do méthode vote qui ajoute dans VoterActual et qui appelle voted et retour error, ...
func (rsa *Ballot) Vote(idAgent string, prefs []int, options []int, w http.ResponseWriter) error {

	rsa.Lock()
	defer rsa.Unlock()

	if rsa.voted(idAgent) {
		err := errors.New("l'agent a déjà voté")
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return nil
	}

	authorized := false
	for _, val := range rsa.voterIds {
		if val == idAgent {
			authorized = true
			break
		}
	}
	if !authorized {
		err := errors.New("l'agent n'est pas autorisé à voter à ce ballot")
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return nil
	}

	//test de deadline
	deadlineTime, err0 := time.Parse(time.RFC3339, rsa.Deadline)
	if err0 != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err0.Error())
		return nil
	}

	currentTime := time.Now()
	rfc3339TimeStr := currentTime.Format(time.RFC3339)
	TimeNow, err1 := time.Parse(time.RFC3339, rfc3339TimeStr)
	if err1 != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err1.Error())
		return nil
	}

	if deadlineTime.Before(TimeNow) {

		err := errors.New("beyond deadline !!!")
		w.WriteHeader(http.StatusServiceUnavailable)
		fmt.Fprint(w, err.Error())
		return nil
	}

	var sl1 = make([]comsoc.Alternative, len(prefs))
	for i, val := range prefs {
		sl1[i] = comsoc.Alternative(val)
	}

	error := comsoc.CheckProfile(sl1, rsa.alts)
	if error != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, error)
		return nil

	}

	rsa.Profil = append(rsa.Profil, sl1)
	rsa.Options = append(rsa.Options, options[0])
	rsa.VoterActual = append(rsa.VoterActual, idAgent)
	log.Printf("[%s] a voté selon %v avec l'option %d \n", idAgent, prefs, options[0])
	return nil

}
