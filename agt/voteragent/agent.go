package agent

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"

	comsoc "gitlab.utc.fr/huchertr/ia04-project/comsoc"
)

type Agent struct {
	idAgent string
	url     string
	Prefs   []comsoc.Alternative
	options []comsoc.Alternative //int
}

func NewAgent(idAgent string, url string, Prefs []comsoc.Alternative, options []comsoc.Alternative) *Agent {
	return &Agent{idAgent, url, Prefs, options}
}

//il leur faut des méthodes pour voter et écouter le serveur

func (rca *Agent) doRequest(idBallot string) (err error) {

	var sl1 = make([]int, len(rca.Prefs))
	var sl2 = make([]int, len(rca.options))

	for i, val := range rca.Prefs {
		sl1[i] = int(val)
	}
	for i, val := range rca.options {
		sl2[i] = int(val)
	}

	req := comsoc.RequestVote{
		IdAgent:  rca.idAgent,
		IdBallot: idBallot,
		Prefs:    sl1,
		Options:  sl2,
	}

	// sérialisation de la requête
	url := rca.url + "/vote"
	data, _ := json.Marshal(req)

	// envoi de la requête
	_, err = http.Post(url, "application/json", bytes.NewBuffer(data))

	if err != nil {
		log.Fatal(rca.idAgent, "error:", err.Error())
	}
	return
}

func (rca *Agent) Start(idBallot string) {
	log.Printf("démarrage de %s", rca.idAgent)
	err := rca.doRequest(idBallot)

	if err != nil {
		log.Fatal(rca.idAgent, "error:", err.Error())
	}
}
