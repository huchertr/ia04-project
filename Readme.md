# Readme

## Comment Installer le projet et lancer l'execution de la demo

faire :
- go get gitlab.utc.fr/huchertr/ia04-project
- go install gitlab.utc.fr/huchertr/ia04-project/test@latest
- ~/go/bin/test
- utiliser l'API avec l'url http://localhost:8080 

La démo crée un ballot 'scrutin1' et fait voter 30 agents

## utilisation 
Nous avons implémenté les méthodes de vote :
- majorité simple : "majority"
- de Borda : "borda"
- par approbation : "approval"
- Condorcet : "condorcet"
- Copeland : "copeland"

## Architecture utilisée


# Serveur 
Nous avons implémenté un serveur. Ce serveur gère une liste de ballot et reçoit des requêtes REST encodées en JSON. En fonction de l'URL de la requête il pourra soit :
 - créer un nouveau ballot 
 - faire voter un agent
 - faire un décompte d'un ballot si les conditions le permette.


# Ballot 
Un ballot a en attribut : 
- id : qui permet de l'identifier
- Rule : qui définit la règle de vote
- Deadline
- voterIds : qui définit les agents autorisés à voter
- VoterActual : ceux qui ont déjà voter
- alts : les alternatives possibles
- TieBreak
- Options
- Profil : le profil actuel

Un ballot a en méthode : 
- NewBallot : pour créer un ballot.
- vote : qui permet à un agent de voter à ce ballot
- voted : qui pour un agent renvoit si il a déjà voter ou non

## Agent 
Un Agent a en attribut : 
- idAgent : qui permet de l'identifier
- url : l'url du serveur
- Prefs : ses préférences
- options
Un Agent a en méthode : 
- NewAgent : pour créer un Agent.
- doRequest : qui permet à un agent faire une requête au serveur (requête de vote uniquement)
- Start : qui écrit dans les logs et fait une requête


