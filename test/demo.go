package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"sync"
	"time"

	agent "gitlab.utc.fr/huchertr/ia04-project/agt/voteragent"
	comsoc "gitlab.utc.fr/huchertr/ia04-project/comsoc"
	server "gitlab.utc.fr/huchertr/ia04-project/srv"
)

var wg sync.WaitGroup

func requeteCreationBallotDemo() {

	var listeAgentVoteur []string
	var mot string
	for i := 0; i < 30; i++ {
		mot = "ag_id" + strconv.Itoa(i+1)
		listeAgentVoteur = append(listeAgentVoteur, mot)
	}
	log.Printf("démarrage de idBallo1")
	req := comsoc.RequestBallot{
		Rule:     "majority",
		Deadline: "2023-11-15T16:42:08+02:00",
		VoterIds: listeAgentVoteur,
		Alts:     3,
		TieBreak: []int{1, 2, 3},
	}

	// sérialisation de la requête
	url := "http://localhost:8080/new_ballot"
	data, _ := json.Marshal(req)

	// envoi de la requête
	_, err := http.Post(url, "application/json", bytes.NewBuffer(data))

	if err != nil {
		log.Fatal("ballot-id1", "error:", err.Error())
	}
}

func initializationAgentPrefs() (prefss [][]comsoc.Alternative, options [][]comsoc.Alternative) {
	// Initialize the random number generator with a seed
	rand.Seed(time.Now().UnixNano())

	// Create a slice of the elements to be permuted
	elements := []comsoc.Alternative{1, 2, 3}
	elementss := make([][]comsoc.Alternative, 30)
	options = make([][]comsoc.Alternative, 30)

	// Shuffle the elements
	for k := 0; k < 30; k++ {
		elementss[k] = make([]comsoc.Alternative, len(elements))
		options[k] = make([]comsoc.Alternative, 1) // taille de options à changer peut etre plus tard
		for i := len(elements) - 1; i > 0; i-- {
			j := rand.Intn(i + 1)
			elements[i], elements[j] = elements[j], elements[i]
		}
		copy(elementss[k], elements)
		options[k][0] = comsoc.Alternative(rand.Intn(len(elements))) // a remplacer par un copy si changement dans options
	}

	return elementss, options
}

func main() {
	const n = 10
	const url1 = ":8080"
	const url2 = "http://localhost:8080"

	clAgts := make([]agent.Agent, 0, n)
	servAgt := server.NewServer(url1)

	//création serveur
	log.Println("démarrage du serveur...")
	go servAgt.Start()
	time.Sleep(time.Second)

	//créer une fausse requête qui crée lance une requete sur le serveur rest et appelle le handler Creation ballot
	wg.Add(1)
	go func() {
		defer wg.Done()
		requeteCreationBallotDemo()
	}()
	wg.Wait()

	//Création des clients
	prefss, optionss := initializationAgentPrefs()
	log.Println("démarrage des clients...")
	for i := 0; i < n; i++ {
		idAgent := fmt.Sprintf("ag_id%d", i+1)
		prefs := prefss[i]
		options := optionss[i]
		agt := agent.NewAgent(idAgent, url2, prefs, options)
		clAgts = append(clAgts, *agt)
	}

	//Vote des clients
	for _, agt := range clAgts {
		// attention, obligation de passer par cette lambda pour faire capturer la valeur de l'itération par la goroutine
		func(agt agent.Agent) {
			go agt.Start("scrutin1")
		}(agt)
	}
	fmt.Println(time.Now())
	fmt.Scanln()
}
