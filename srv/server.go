package server

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"sync"
	"time"

	ballot "gitlab.utc.fr/huchertr/ia04-project/agt/ballotagent"
	comsoc "gitlab.utc.fr/huchertr/ia04-project/comsoc"
)

type Server struct {
	sync.RWMutex
	id         string
	addr       string
	listBallot map[string]*ballot.Ballot
}

func NewServer(addr string) *Server {
	listBallot := make(map[string]*ballot.Ballot)
	return &Server{id: addr, addr: addr, listBallot: listBallot}
}

func (*Server) RequestDecompte(r *http.Request) (req comsoc.RequestVote, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

func (*Server) decodeRequestVote(r *http.Request) (req comsoc.RequestVote, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

func (*Server) decodeRequestBallot(r *http.Request) (req comsoc.RequestBallot, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

// Test de la méthode
func (rsa *Server) checkMethod(method string, w http.ResponseWriter, r *http.Request) bool {
	if r.Method != method {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "method %q not allowed", r.Method)
		return false
	}
	return true
}

// il faut changer les arguments pour les passer via Request_ballot (le serveur)
func (rsa *Server) CreationBallot(w http.ResponseWriter, r *http.Request) {

	rsa.RLock()
	defer rsa.RUnlock()

	// vérification de la méthode de la requête
	if !rsa.checkMethod("POST", w, r) {
		return
	}

	// décodage de la requête
	req, err := rsa.decodeRequestBallot(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}
	if (req.Rule != "majority") && (req.Rule != "condorcet") && (req.Rule != "approval") && (req.Rule != "borda") && (req.Rule != "copeland") {
		err0 := errors.New("pas une méthode de vote implémenté")
		w.WriteHeader(http.StatusNotImplemented)
		fmt.Fprint(w, err0.Error())
		return
	}
	currentTime := time.Now()
	if req.Deadline < currentTime.Format(time.RFC3339) {
		err2 := errors.New("deadline déja passée")
		w.WriteHeader(http.StatusNotImplemented)
		fmt.Fprint(w, err2.Error())
		return
	}

	taillePlus1 := len(rsa.listBallot) + 1
	idBallot := "scrutin" + strconv.Itoa(taillePlus1)

	NewBallot := ballot.NewBallot(idBallot, req.Rule, req.Deadline, req.VoterIds, req.Alts, req.TieBreak)
	log.Println(idBallot, "créé")
	rsa.listBallot[idBallot] = NewBallot
	log.Println(idBallot, "ajouté à la liste des scrutins")

	//to do retour de la requete (http)
	var resp comsoc.ResponseBallot
	resp.IdBallot = idBallot

	//envoie de la réponse au serveur rest
	w.WriteHeader(http.StatusCreated)
	serial, _ := json.Marshal(resp)
	w.Write(serial)
}

func (rsa *Server) doVote(w http.ResponseWriter, r *http.Request) {

	// vérification de la méthode de la requête
	if !rsa.checkMethod("POST", w, r) {
		return
	}

	// décodage de la requête
	req, err := rsa.decodeRequestVote(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	//check que le ballot pour lequel on vote existe
	if rsa.listBallot[req.IdBallot] == nil {
		w.WriteHeader(http.StatusNotImplemented)
		msg := fmt.Sprintf("Error : ce scrutin n'existe pas")
		w.Write([]byte(msg))
		return
	}

	//vote de l'agent
	error := rsa.listBallot[req.IdBallot].Vote(req.IdAgent, req.Prefs, req.Options, w)
	if error != nil {
		w.WriteHeader(http.StatusNotImplemented)
		msg := fmt.Sprintf("Error '%s'", error) //essayer fmt.Fprint si ca passe pas de mettre error dans un w.Write
		w.Write([]byte(msg))
		return

	}
}

func (rsa *Server) doDecompte(w http.ResponseWriter, r *http.Request) {

	// vérification de la méthode de la requête
	if !rsa.checkMethod("POST", w, r) {
		return
	}

	// décodage de la requête
	req, err1 := rsa.RequestDecompte(r)
	if err1 != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err1.Error())
		return
	}

	//check si le ballot existe
	authorized := false
	for i, _ := range rsa.listBallot {
		if i == req.IdBallot {
			authorized = true
			break
		}
	}
	if !authorized {
		err := errors.New("Ce scrutin n'existe pas")
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprint(w, err.Error())
		return
	}

	ballot := rsa.listBallot[req.IdBallot]

	//test de deadline
	deadlineTime, err0 := time.Parse(time.RFC3339, ballot.Deadline)
	if err0 != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err0.Error())
		return
	}

	if !deadlineTime.Before(time.Now()) {
		err := errors.New("before deadline !!!")
		w.WriteHeader(425)
		fmt.Fprint(w, err.Error())
		return
	}

	var sl1 = make([]comsoc.Alternative, len(ballot.TieBreak))
	for i, val := range ballot.TieBreak {
		sl1[i] = comsoc.Alternative(val)
	}

	var bestAlts []comsoc.Alternative
	var bestAlt comsoc.Alternative
	var err error
	log.Println("Decompte selon ", ballot.Rule, "\n")

	if len(ballot.VoterActual) == 0 {
		err := errors.New("pas encore de votant")
		w.WriteHeader(200)
		fmt.Fprint(w, err.Error())
		return
	}

	switch ballot.Rule {
	case "majority":
		funcTB := comsoc.TieBreakFactory(sl1)
		scfNoEx := comsoc.SCFFactory(comsoc.MajoritySCF, funcTB)
		bestAlt, err = scfNoEx(ballot.Profil)
	case "borda":
		funcTB := comsoc.TieBreakFactory(sl1)
		scfNoEx := comsoc.SCFFactory(comsoc.BordaSCF, funcTB)
		bestAlt, err = scfNoEx(ballot.Profil)
	case "approval":
		if len(ballot.Profil) != len(ballot.Options) {
			log.Println("impossible de faire le vote par approbation car certains agent n'ont pas préciser leur option")
			return
		}
		funcTB := comsoc.TieBreakFactory(sl1)
		bestAlts, err = comsoc.ApprovalSCF(ballot.Profil, ballot.Options)
		if err != nil {
			log.Println(err)
			return
		}
		if len(bestAlts) == 1 {
			bestAlt = bestAlts[0]
		} else {
			bestAlt, err = funcTB(bestAlts)
			if err != nil {
				log.Println(err)
				return
			}
		}
	case "condorcet":
		bestAlts, err = comsoc.CondorcetWinner(ballot.Profil)
		if err != nil {
			log.Println(err)
			return
		}
		bestAlt = bestAlts[0]
	case "copeland":
		funcTB := comsoc.TieBreakFactory(sl1)
		scfNoEx := comsoc.SCFFactory(comsoc.CopelandSCF, funcTB)
		bestAlt, err = scfNoEx(ballot.Profil)
	}
	if err != nil {
		log.Println(err)
		return
	}

	//to do retour de la requete (http)
	var resp comsoc.ResponseDecompte
	resp.Winner = int(bestAlt)

	//envoie de la réponse au serveur rest
	w.WriteHeader(http.StatusOK)
	serial, _ := json.Marshal(resp)
	w.Write(serial)

}

func (rsa *Server) Start() {
	// création du multiplexer
	mux := http.NewServeMux()
	mux.HandleFunc("/new_ballot", rsa.CreationBallot)
	mux.HandleFunc("/vote", rsa.doVote)
	mux.HandleFunc("/result", rsa.doDecompte)

	// création du serveur http
	s := &http.Server{
		Addr:           rsa.addr,
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20}

	// lancement du serveur
	log.Println("Listening on", rsa.addr)
	go log.Fatal(s.ListenAndServe())
}
